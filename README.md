# LGB Meal Master NEA

This is my College NEA project that I was allocated about 6 months to theorize and complete. I then had a month to write a full report on the entirity of the code and demonstrate my work.
Overall I'm happy with the project as my first piece of commited work, though I have since learned wild amounts.

The report on this project can be viewed here: [Layton_Burchell_-_NEA_Report.pdf](https://gitlab.com/LaytonGB/lgb-meal-master-nea/-/raw/master/Layton_Burchell_-_NEA_Report.pdf?inline=false)
