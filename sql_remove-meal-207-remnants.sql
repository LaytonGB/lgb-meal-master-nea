/*
SELECT *
FROM meal_stepssection
LEFT JOIN meal_step ON meal_stepssection.id=meal_step.section_id
LEFT JOIN meal_subcomment ON meal_step.id=meal_subcomment.parent_id
LEFT JOIN meal_subcommentratingsection ON meal_subcomment.id=meal_subcommentratingsection.parent_id
LEFT JOIN meal_subcommentrating ON meal_subcommentratingsection.id=meal_subcommentrating.parent_id
WHERE meal_stepssection.meal_id=370;

SELECT *
FROM meal_mealratingsection
LEFT JOIN meal_mealrating ON meal_mealratingsection.id=meal_mealrating.parent_id
WHERE meal_mealratingsection.parent_id=370;
*/

DELETE
FROM meal_sourcedetails
WHERE meal_sourcedetails.meal_id>370 AND meal_id<391;

DELETE
FROM meal_meal_favourites
WHERE meal_meal_favourites.meal_id>370 AND meal_id<391;

DELETE
FROM meal_mealcomment
WHERE meal_mealcomment.meal_id>370 AND meal_id<391;

DELETE
FROM accounts_plannermeal
WHERE accounts_plannermeal.meal_id>370 AND meal_id<391;

DELETE FROM meal_subcommentrating WHERE parent_id IN (
    SELECT id FROM meal_subcommentratingsection WHERE parent_id IN (
        SELECT id FROM meal_subcomment WHERE parent_id IN (
            SELECT id FROM meal_step WHERE section_id IN (
                SELECT id FROM meal_stepssection WHERE meal_id>370 AND meal_id<391
            )
        )
    )
);
DELETE FROM meal_subcommentratingsection WHERE parent_id IN (
    SELECT id FROM meal_subcomment WHERE parent_id IN (
        SELECT id FROM meal_step WHERE section_id IN (
            SELECT id FROM meal_stepssection WHERE meal_id>370 AND meal_id<391
        )
    )
);
DELETE FROM meal_subcomment WHERE parent_id IN (
    SELECT id FROM meal_step WHERE section_id IN (
        SELECT id FROM meal_stepssection WHERE meal_id>370 AND meal_id<391
    )
);
DELETE FROM meal_step WHERE section_id IN (
    SELECT id FROM meal_stepssection WHERE meal_id>370 AND meal_id<391
);
DELETE FROM meal_stepssection WHERE meal_id>370 AND meal_id<391;

DELETE FROM meal_mealrating WHERE parent_id IN (
    SELECT id FROM meal_mealratingsection WHERE parent_id>370 AND parent_id<391
);
DELETE FROM meal_mealratingsection WHERE parent_id>370 AND parent_id<391;
