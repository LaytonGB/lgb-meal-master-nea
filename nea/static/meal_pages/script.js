let meal_comments_sort;

window.addEventListener('load', function () {
    // Ratings Script from https://codepen.io/lsirivong/pen/ekBxI
    if (overall_user_rated) fillStarsToNumber(+overall_rating, true);
    else fillStarsToNumber(+overall_rating, false);

    function fillStarsToNumber(lim, gold) {
        const ratingButtons = document.getElementsByClassName('rating-star');
        const fillType = gold ? 'highlight' : 'fill';
        for (let i = ratingButtons.length - 1; i >= 5 - lim; i--) {
            // add class to each star to meet average votes
            ratingButtons[i].classList.toggle(fillType);
        }
    }

    function forEachInCollection(collection, func) {
        for (let i = 0; i < collection.length; i++) {
            func(collection[i]);
        }
    }

    meal_comments_sort = new Vue({
        delimiters: ['[[', ']]'],
        el: '#meal_page',
        data: {
            comments: comments,
            comment_sort: 'date',
            comment_sort_key: 0,
            ingredients_vue: ingredients_vue,
            instructions_vue: instructions_vue,
            user_id: user_id,
            user_is_authenticated: user_is_authenticated,
            csrf_string: csrf_string,
            is_fave: is_fave,
        },
        computed: {
            sortedComments: function () {
                const compVal = this.comment_sort;
                return myFunctions.sort(this.comments, compVal, true);
            },
            ingredients_sorted: function () {
                const arr = Array.from(this.ingredients_vue); // create copy of data
                let compVal;
                arr.forEach(s => { // for each step
                    compVal = s.comment_sort; // set compare variable
                    s.comments = myFunctions.sort(s.comments, compVal, true); // sort the comments
                });
                return arr; // return sorted array
            },
            instructions_sorted: function () {
                const arr = Array.from(this.instructions_vue); // create copy of data
                let compVal;
                arr.forEach(s => { // for each step
                    compVal = s.comment_sort; // set compare variable
                    s.comments = myFunctions.sort(s.comments, compVal, true); // sort the comments
                });
                return arr; // return sorted array
            },
        },
        methods: {
            hasRatedUp: function (i) {
                const r = this.comments.find(c => c.comment_id === i).user_rating;
                if (r === 1) return 'text-warning';
                return 'text-muted';
            },
            hasRatedDown: function (i) {
                const r = this.comments.find(c => c.comment_id === i).user_rating;
                if (r === -1) return 'text-warning';
                return 'text-muted';
            },
            hasRatedIngredientSubcommentUp: function (i, j) {
                const r = this.ingredients_vue[i].comments[j].user_rating;
                if (r === 1) return 'text-warning';
                return 'text-muted';
            },
            hasRatedIngredientSubcommentDown: function (i, j) {
                const r = this.ingredients_vue[i].comments[j].user_rating;
                if (r === -1) return 'text-warning';
                return 'text-muted';
            },
            hasRatedInstructionSubcommentUp: function (i, j) {
                const r = this.instructions_vue[i].comments[j].user_rating;
                if (r === 1) return 'text-warning';
                return 'text-muted';
            },
            hasRatedInstructionSubcommentDown: function (i, j) {
                const r = this.instructions_vue[i].comments[j].user_rating;
                if (r === -1) return 'text-warning';
                return 'text-muted';
            },
            closeOtherSections: function (el) {
                const cont = document.getElementsByClassName('content');
                for (let i = 0; i < cont.length; i++) {
                    if (el !== cont[i]) {
                        cont[i].style.display = 'none';
                    } else {
                        if (cont[i].style.display === 'none')
                            cont[i].style.display = '';
                        else cont[i].style.display = 'none';
                    }
                }
            },
            setCsrf: function () {
                const csrf_list = document.getElementsByClassName(
                    'csrf_section'
                );
                forEachInCollection(csrf_list, el => {
                    el.setAttribute('value', csrf_string);
                });
            },
        },
        watch: {
            csrf_string: function () {
                this.setCsrf();
            },
        },
        mounted: function () {
            // set the csrf tokens
            this.setCsrf();

            // Get the collapsible steps
            const coll = document.getElementsByClassName('collapsible');
            // Open the appropriate comments section if a comment was left before page refresh
            if (step_id) {
                const editedStep = document.getElementById('step_' + step_id);
                this.closeOtherSections(editedStep);
            }
            // open them on click
            for (let i = 0; i < coll.length; i++) {
                coll[i].addEventListener('click', function () {
                    const content = this.nextElementSibling;
                    meal_comments_sort.closeOtherSections(content);
                });
            }

            // scroll to the specified ID
            if (scroll_to) {
                const scrollEl = document.getElementById(scroll_to);
                if (scrollEl) scrollEl.scrollIntoView({ block: 'center' });
            }
        },
    });
});
