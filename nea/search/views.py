from django.http.response import JsonResponse
import requests
from bs4 import BeautifulSoup

from django.shortcuts import render

from .forms import MealSearchForm
from meal.models import Meal

# Return full page of results
def search_page(request):
    form = MealSearchForm()
    context = {
        'page_title':'Search',
        'form':form,
        'has_searched':False,
        'results_vue': [],
    }

    if request.method == 'GET':
        form = MealSearchForm(request.GET)

        if form.is_valid():
            has_searched = True
            search_term = form.cleaned_data.get('search_term')

            local_results = Meal.objects.filter(name__icontains=search_term)
            results = list(local_results)

            if len(results) < 15: # if less than 15 results were fetched from the database, scrape BBC Good Foods
                search_page = 0
                meal_count = 0
                while len(results) < 25: # search until 25 results are found or 5 pages of results have been searched
                    if search_page > 5:
                        break

                    print('=== BBC GOOD FOODS BEING ACCESSED - PAGE ' + str(search_page) + ' ===')
                    search_url = 'https://www.bbcgoodfood.com/search/recipes/page/%s/?q=%s&sort=-relevance' % (search_page, search_term)
                    print(search_url)

                    page = requests.get(search_url, headers={'User-Agent':'Chrome/84'})
                    soup = BeautifulSoup(page.content, 'html.parser') # feed page data into beautiful soup

										# meals are listed on the search results page under the tag "h4"
                    meals = soup.find_all('h4', class_='standard-card-new__display-title')
                    print('Meals found on this page: ' + str(len(meals)))
                    if len(meals) == 0: # If no meals are on the page, stop searching
                        break

                    for result in meals:
                        # Get the page for each meal
                        meal_url = str(result.a.get('href'))

                        if meal_url.find('recipes') == -1: # do not include adverts
                            break

                        result_url = 'https://www.bbcgoodfood.com' + meal_url

												# check to see if a meal has already been added to the database with this url using the meal_sourcedetails table
                        try:
                            Meal.sourcedetails.get(source=result_url)
												# If the meal has not been added, get its name
                        except:
                            meal_page = requests.get(result_url, headers={'User-Agent':'Chrome/84'})
                            meal_soup = BeautifulSoup(meal_page.content, 'html.parser')

                            # Get the meal NAME
                            meal_name = meal_soup.h1.get_text('', strip=True)

                            for result in results:

                                # if the meal is already in the list, stop
                                if result.name == meal_name:
                                    break
                            else:
                                try:
                                    # If a meal with this name already exists, add it to results
                                    results.append(Meal.objects.get(name=meal_name))

                                except:
                                    try:
                                        meal_count += 1
                                        print(f'Adding meal {meal_count}...')

                                        # Else create the meal then add it
                                        # Get the meal DESCRIPTION
                                        meal_description_raw = meal_soup.find('section', class_='post-header').find('div', attrs={'class':'editor-content'})
                                        meal_description = meal_description_raw.get_text('', strip=False)

                                        # Get the meal INGREDIENTS
                                        meal_ingredients = []
                                        for ingredient in meal_soup.find('section', class_='recipe__ingredients').find_all('li', class_='list-item'):
                                            meal_ingredients.append(ingredient.get_text(' ', strip=False))

                                        # Get the meal INSTRUCTIONS
                                        meal_instructions = []
                                        for instruction in meal_soup.find('section', class_='recipe__method-steps').find_all('div', class_='editor-content'):
                                            meal_instructions.append(instruction.p.get_text(' ', strip=False))

                                        # Create and Save the meal
                                        results.append(
                                            Meal.objects.create_meal(meal_name, meal_description, meal_ingredients, meal_instructions, False, result_url)
                                        )

                                        print(f'Meal {meal_count} added.')
                                    except:
                                        print(f'Something went wrong and a meal object could not be created for meal "{meal_name}".')
                                        break

                    search_page += 1

            # prep vue variables
            results_vue = []
            for result in results:
                rating_section = result.ratingsection.filter(name='overall').first()
                results_vue.append({
                    'name': result.name,
                    'id': result.id,
                    'rating': rating_section.get_rating(),
                    'rating_count': rating_section.rating_count(),
                    'description': result.description,
                    'ingredients': result.ingredients(),
                })

            context = {
                'page_title':'Search',
                'form':form,
                'results':results,
                'results_vue':results_vue,
                'has_searched':has_searched,
            }
            return render(request, 'search/main.html', context)

    return render(request, 'search/main.html', context)

# return results of search via JSON, without scraping BBC Good Foods
def search_results(request, search_term=None):
    if not search_term:
        return JsonResponse([], safe=False)
    local_results = list(Meal.objects.filter(name__icontains=search_term))
    results = []
    for r in local_results:
        rating_section = r.ratingsection.filter(name='overall').first()
        results.append({
            'id': r.id,
            'name': r.name,
            'description': r.description,
            'rating': rating_section.get_rating(),
            'rating_count': rating_section.rating_count(),
        })
    return JsonResponse(results, safe=False)
