from django import forms

class MealSearchForm(forms.Form):
    search_term = forms.CharField(
        label = 'Search',
        max_length = 64,
        widget = forms.TextInput(attrs={'id':'search_field', 'class':'form-control', 'placeholder':'Search',})
    )
