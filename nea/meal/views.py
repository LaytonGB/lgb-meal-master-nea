import json
from django.core.serializers.json import DjangoJSONEncoder # for json serialization of datetime

from django.shortcuts import render, redirect
from django.contrib import messages

from .models import Meal, StepsSection, Step, MealComment, SubComment, MealRating, CommentRating, SubCommentRating
from .forms import CommentCreationForm

def meal_page(request, meal_id, step_id=None, focus=None):
    form = CommentCreationForm(auto_id=False)
    user = request.user

    meal = Meal.objects.get(pk=meal_id)
    meal_name = meal.name
    description = meal.description

    # get the overall rating section rating
    overall_user_rated = False
    main_rating_section, created = meal.ratingsection.get_or_create(
        name='overall',
        defaults={'meal':meal},
    )
    overall_rating = main_rating_section.get_rating()
    try:
        this_user_rating = main_rating_section.rating.filter(user=user).first()
    except:
        this_user_rating = None
    if this_user_rating is not None:
        overall_rating = this_user_rating.value
        overall_user_rated = True
    if overall_rating == -1:
        overall_rating = 0

    # get other rating sections
    alt_rating_sections = meal.ratingsection.exclude(name='overall')
    user_ratings = {}
    for sec in alt_rating_sections:
        user_rating = sec.rating.filter(user=user).first()
        if user_rating is not None:
            user_ratings[sec.name] = user_rating.value

    # Create instructions and ingredients
    ingredients_section = meal.stepssection.get(name='Ingredients')
    ingredients = ingredients_section.step.all()
    instructions_section = meal.stepssection.get(name='Instructions')
    instructions = instructions_section.step.all()

    #create vue required variables for subcomment sorting in the above sections
    def get_vue_steps(step_section):
        out = []
        for step in step_section:
            j = []
            for comment in step.subcomment.all():
                try:
                    ur = comment.ratingsection.rating.filter(user=request.user).first().value
                except:
                    ur = 0
                j.append({
                    'username': comment.user.username,
                    'user_id': comment.user.pk,
                    'content': comment.content,
                    'date': comment.date.strftime("%d/%m/%Y, %H:%M:%S"),
                    'id': comment.pk,
                    'rating': comment.ratingsection.get_rating(),
                    'type': comment.c_name(),
                    'user_rating': ur,
                })
            i = {
                'content': step.content,
                'id': step.pk,
                'comments': j,
                'comment_count': len(step.subcomment.all()),
                'comment_sort': 'rating',
                'key': 0,
            }
            out.append(i)
        return out
    ingredients_vue = get_vue_steps(ingredients)
    instructions_vue = get_vue_steps(instructions)

    # Get comments and their sort type
    comments_sort = 'date'
    comments = meal.mealcomment.order_by(comments_sort)

    # get data for vue
    comment_objects = []
    for c in comments:
        try:
            user_rating = c.ratingsection.rating.filter(user=request.user).first().value
        except:
            user_rating = 0
        comment_objects.append({
            'user_id': c.user.pk,
            'username': c.user.username,
            'comment_id': c.pk,
            'comment_type': c.c_name(),
            'date': c.date,
            'datestring': c.date.strftime("%d/%m/%Y, %H:%M:%S"),
            'content': c.content,
            'rating': c.ratingsection.get_rating(),
            'user_rating': user_rating,
        })
    json_dump = json.dumps(
        comment_objects,
        cls=DjangoJSONEncoder
    )

    # Get list of all user-rated comments for this meal
    rated_comments = []
    try:
        comment_ratings = request.user.commentrating.filter(parent__parent__meal__pk=meal_id)
        for rating in comment_ratings:
            comment = rating.parent.parent
            rated_comments += [[comment.pk, rating.value]]
    except:
        pass
    rated_subcomments = []
    try:
        subcomment_ratings = request.user.subcommentrating.filter(parent__parent__parent__section__meal__pk=meal_id)
        for rating in subcomment_ratings:
            step = rating.parent.parent.parent
            comment = rating.parent.parent
            rated_subcomments += [[comment.pk, rating.value]]
    except:
        pass

    # get scroll focus
    if focus is not None:
        scroll_focus_id = focus
    else:
        scroll_focus_id = ''

    try:
        is_fave = meal.favourites.get(id=user.id) is not None
    except:
        is_fave = False

    # Declare context
    context = {
        'form':form,
        'meal':meal,
        'page_title':meal_name,
        'meal_name':meal_name,
        'description': description,
        'ingredients':ingredients,
        'ingredients_vue':ingredients_vue,
        'instructions':instructions,
        'instructions_vue':instructions_vue,
        'comments':comments,
        'comments_sort':comments_sort,
        'overall_rating':overall_rating,
        'overall_user_rated':overall_user_rated,
        'alt_rating_sections':alt_rating_sections,
        'user_rated':user_ratings, # { [id: string]: [this_user's_rating_value: int] }
        'rated_comments':rated_comments, # a queryset of comments this user has rated on this meal
        'rated_subcomments':rated_subcomments,
        'step_id':step_id,
        'json_data':json_dump,
        'scroll_to':scroll_focus_id,
        'is_fave':is_fave,
        'user_id':user.pk or 'undefined',
    }

    return render(request, 'meal_pages/meal_details.html', context)

def submit_meal_rating(request, meal_id, rating_section_name):
    if not request.user.is_authenticated:
        return redirect('/account/login/')
    if request.method == 'POST':
        for num in [0,1,2,3,4,5]:
            rating_name = 'rating-' + str(num)
            if rating_name in request.POST:
                meal = Meal.objects.get(pk=meal_id)
                rating_section = meal.ratingsection.filter(name=rating_section_name).first()
                user_rating = rating_section.rating.filter(user=request.user).first()
                if user_rating is not None:
                    print('user rating exists')
                    if user_rating.value == num:
                        user_rating.delete()
                    else:
                        setattr(user_rating, 'value', num)
                        user_rating.save()
                else:
                    MealRating.objects.create_rating(rating_section, request.user, num)
                break
        else:
            messages.error(request, 'Meal rating failed.')
    return redirect('/meal/%s/' % meal_id)

def comment(request, meal_id):
    if request.method == 'POST':
        form = CommentCreationForm(request.POST)

        if form.is_valid():
            print('FORM WAS VALID')
            meal = Meal.objects.get(pk=meal_id)
            content = form.cleaned_data.get('content')
            user = request.user
            MealComment.objects.create_comment(meal, user, content)
        else:
            messages.danger(request, 'Comment was erroneous! How did you do that?')

    return redirect('/meal/%s/comment_section/' % meal_id)

def submit_comment_rating(request, meal_id, comment_id):
    if request.method == 'POST':
        meal_comment = MealComment.objects.get(pk=comment_id)
        rating_section = meal_comment.ratingsection
        user_rating = rating_section.rating.filter(user=request.user).first()
        if '1' in request.POST:
            user_vote = 1
        elif '-1' in request.POST:
            user_vote = -1
        else: # just in case no value was supplied - should never get reached
            user_vote = 0
        if user_rating is not None:
            current_rating = user_rating.value
            if current_rating == user_vote:
                user_rating.delete()
            else:
                setattr(user_rating, 'value', user_vote)
                user_rating.save()
        else:
            CommentRating.objects.create_rating(rating_section, request.user, user_vote)
    return redirect('/meal/%s/comment_%s/' % (meal_id, comment_id))

def subcomment(request, meal_id, step_id):
    if request.method == 'POST':
        form = CommentCreationForm(request.POST)

        if form.is_valid():
            step = Step.objects.get(pk=step_id)
            user = request.user
            content = form.cleaned_data.get('content')
            SubComment.objects.create_subcomment(step, user, content)
        else:
            messages.danger(request, 'Comment was erroneous! How did you do that?')
    return redirect('/meal/%s/step_%s/%s/' % (meal_id, step_id, step_id, ))

def submit_subcomment_rating(request, meal_id, step_id, subcomment_id):
    if request.method == 'POST':
        sub_comment = SubComment.objects.get(pk=subcomment_id)
        rating_section = sub_comment.ratingsection
        user_rating = rating_section.rating.filter(user=request.user).first()
        if '1' in request.POST:
            user_vote = 1
        elif '-1' in request.POST:
            user_vote = -1
        else: # just in case no value was supplied - should never get reached
            user_vote = 0
        if user_rating is not None:
            current_rating = user_rating.value
            if current_rating == user_vote:
                user_rating.delete()
            else:
                setattr(user_rating, 'value', user_vote)
                user_rating.save()
        else:
            SubCommentRating.objects.create_rating(rating_section, request.user, user_vote)
    return redirect('/meal/%s/subcomment_rating_%s/%s/' % (meal_id, subcomment_id, step_id, ))
