from django.contrib import admin

from .models import Meal, SourceDetails, StepsSection, Step
from .models import MealRatingSection, MealRating, MealComment, SubComment
from .models import CommentRatingSection, CommentRating, SubCommentRatingSection, SubCommentRating

admin.site.site_header='Meal Master Administration'
admin.site.site_title='Meal Master Administration'
admin.site.index_title='Meal Master Administration'

# Solution for multi-level inlining found at https://stackoverflow.com/questions/14308050/django-admin-nested-inline

class FavouritesInline(admin.TabularInline):
    model = Meal.favourites.through
    verbose_name = 'Favourite'
    verbose_name_plural = 'Favourites'
    extra = 0

class MealDetailsInline(admin.TabularInline):
    model = SourceDetails
    verbose_name = 'Source Details'
    verbose_name_plural = 'Source Details'
    extra = 0

class AbstractRatingInline(admin.TabularInline):
    verbose_name = 'Rating'
    fields = [
        'user',
        'value',
    ]
    readonly_fields = [
        'parent',
        'user',
        'value',
    ]
    extra = 0

class MealRatingInline(AbstractRatingInline):
    model = MealRating
class MealRatingSectionAdmin(admin.ModelAdmin):
    inlines = [MealRatingInline]
admin.site.register(MealRatingSection, MealRatingSectionAdmin)

class CommentRatingInline(AbstractRatingInline):
    model = CommentRating
class CommentRatingSectionAdmin(admin.ModelAdmin):
    inlines = [CommentRatingInline]
admin.site.register(CommentRatingSection, CommentRatingSectionAdmin)

class AbstractRatingSectionInlineLink(admin.TabularInline):
    verbose_name = 'Rating Section'
    fields = [
        'name',
        'rating_count',
    ]
    readonly_fields = [
        'name',
        'rating_count',
    ]
    show_change_link = True
    extra = 0

class CommentRatingSectionInlineLink(AbstractRatingSectionInlineLink):
    model = CommentRatingSection
class MealCommentAdmin(admin.ModelAdmin):
    inlines = [CommentRatingSectionInlineLink]
admin.site.register(MealComment, MealCommentAdmin)

class SubCommentRatingInline(admin.TabularInline):
    model = SubCommentRating
    verbose_name = 'Sub-Comment Rating'
    fields = [
        'parent',
        'user',
        'value',
    ]
    readonly_fields = [
        'parent',
        'user',
        'value',
    ]
    extra = 0
class SubCommentRatingSectionAdmin(admin.ModelAdmin):
    inlines = [SubCommentRatingInline]
admin.site.register(SubCommentRatingSection, SubCommentRatingSectionAdmin)

class SubCommentRatingSectionInlineLink(AbstractRatingSectionInlineLink):
    model = SubCommentRatingSection
class SubCommentAdmin(admin.ModelAdmin):
    inlines = [SubCommentRatingSectionInlineLink]
admin.site.register(SubComment, SubCommentAdmin)

class SubCommentInlineLink(admin.TabularInline):
    model = SubComment
    verbose_name = 'Sub-Comment'
    fields = [
        'parent',
        'user',
        'date',
        'content',
    ]
    readonly_fields = [
        'parent',
        'user',
        'date',
        'content',
    ]
    show_change_link = True
    extra = 0
class StepAdmin(admin.ModelAdmin):
    inlines = [SubCommentInlineLink]
admin.site.register(Step, StepAdmin)

class StepLinkInline(admin.TabularInline):
    model = Step
    verbose_name = 'Step Model'
    fields = [
        'content',
        'comment_count',
    ]
    readonly_fields = [
        'comment_count',
    ]
    show_change_link = True
    extra = 0
class StepsSectionAdmin(admin.ModelAdmin):
    inlines = [StepLinkInline]
admin.site.register(StepsSection, StepsSectionAdmin)

class StepsSectionLinkInline(admin.TabularInline):
    model = StepsSection
    verbose_name = 'Steps Section'
    fields = [
        'name',
    ]
    readonly_fields = [
        'meal',
    ]
    show_change_link = True
    extra = 0

class MealRatingSectionInlineLink(AbstractRatingSectionInlineLink):
    model = MealRatingSection
class MealCommentInlineLink(admin.TabularInline):
    model = MealComment
    verbose_name = 'Comment'
    readonly_fields = [
        'meal',
        'user',
        'content',
        'rating_count',
    ]
    show_change_link = True
    extra = 0
class MealAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {"fields": ([
                    'name',
                    'description',
                ]),
        }),
    )
    inlines = [MealDetailsInline, FavouritesInline, MealRatingSectionInlineLink, MealCommentInlineLink, StepsSectionLinkInline]
admin.site.register(Meal, MealAdmin)
