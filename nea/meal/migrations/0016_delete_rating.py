# Generated by Django 3.1 on 2020-09-01 11:20

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('meal', '0015_auto_20200901_1023'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Rating',
        ),
    ]
