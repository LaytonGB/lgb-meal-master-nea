# Generated by Django 3.1.1 on 2020-10-05 10:40

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('meal', '0024_meal_favourites'),
    ]

    operations = [
        migrations.AlterField(
            model_name='meal',
            name='favourites',
            field=models.ManyToManyField(related_name='Favourites', to=settings.AUTH_USER_MODEL),
        ),
    ]
