# Generated by Django 3.1.1 on 2020-10-20 11:45

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('meal', '0025_auto_20201005_1040'),
    ]

    operations = [
        migrations.AlterField(
            model_name='meal',
            name='favourites',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL),
        ),
    ]
