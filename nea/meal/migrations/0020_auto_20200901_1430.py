# Generated by Django 3.1 on 2020-09-01 14:30

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('meal', '0019_auto_20200901_1416'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Rating',
            new_name='MealRating',
        ),
    ]
