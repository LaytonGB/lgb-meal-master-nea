from math import floor, log10
import re

from django.db import models
from django.contrib.auth.models import User
from django.db.models.aggregates import Avg, Sum
from django.db.models.deletion import CASCADE
from django.db.models.expressions import Func
# from django.utils.safestring import mark_safe # Allows <a> tags in Django admin area https://stackoverflow.com/questions/47953705/how-do-i-use-allow-tags-in-django-2-0-admin/47953774

from django.urls import reverse
from functools import reduce

# Aggregation rounding function
class Rnd(Func):
    function = 'ROUND'
    template='%(function)s(%(expressions)s)'

# Model and manager of a meal
class MealManager(models.Manager):
    def create_meal(self, name, description, ingredients, instructions, user_created, url_or_userid):
        meal = self.create(
            name = name,
            description = description,
        )
        StepsSection.objects.create_stepssection(meal, 'Ingredients', ingredients) # create the ingredients rows for this meal
        StepsSection.objects.create_stepssection(meal, 'Instructions', instructions) # create the instructions rows for this meal
        SourceDetails.objects.create_sourcedetails(meal, user_created, url_or_userid) # create the source details for this meal
        MealRatingSection.objects.create_ratingsection('overall', meal) # create the overall rating section for this meal
        return meal

class Meal(models.Model):
    name = models.CharField(unique=True, max_length=200,)
    description = models.TextField(default='No description... yet.')
    favourites = models.ManyToManyField(User)
    # a function that can be used to return all the ingredients in this meal
    # the ingredients are listed using the regex in the "step" model
    def ingredients(self):
        stepssections = self.stepssection.all()
        ingredients = []
        for stepssection in stepssections:
            if stepssection.name == 'Ingredients':
                ingredients.extend(stepssection.ingredients())
        return ingredients
    def __str__(self): # the format of the name for this model in the admin section
        return '(Meal ID: %s) Meal %s' % (self.id, self.name)
    class Meta:
        ordering = ('pk',) # how to order these models by default
    objects = MealManager() # assign the manager of this model to the model

# Model and manager for the details of a meal's source
class SourceDetailsManager(models.Manager):
    def create_sourcedetails(self, meal, user_created, url_or_userid):
        if url_or_userid is int:
            creator = url_or_userid
            source_details = self.create(meal=meal, user_created=user_created, creator=creator)
        else:
            source = url_or_userid
            source_details = self.create(meal=meal, user_created=user_created, source=source)
        return source_details

class SourceDetails(models.Model):
    meal = models.OneToOneField(Meal, on_delete=models.CASCADE, related_name='sourcedetails')
    user_created = models.BooleanField()
    date = models.DateTimeField(auto_now=True)
    creator = models.IntegerField(null=True, blank=True)
    source = models.URLField(null=True, blank=True)
    def __str__(self):
        return 'Source Details for ' + self.meal.__str__()
    objects = SourceDetailsManager()

# Model and manager for a meal's primary comments section
class MealCommentManager(models.Manager):
    def create_comment(self, meal, user, content):
        comment = self.create(
            meal = meal,
            user = user,
            content = content,
        )
        CommentRatingSection.objects.create_ratingsection('overall', comment)

class MealComment(models.Model):
    meal = models.ForeignKey(Meal, on_delete=models.CASCADE, related_name='mealcomment')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='mealcomment')
    date = models.DateTimeField(auto_now_add=True)
    content = models.TextField()
    def rating_count(self):
        return len(self.ratingsection.rating.all())
    def __str__(self):
        return 'Comment on %s (%s)' % (self.meal.__str__(), self.id)
    def c_name(self):
        return 'mealcomment'
    class Meta:
        ordering = ('date',)
    objects = MealCommentManager()

# Model and manager for an ingredients or instructions section
class StepsSectionManager(models.Manager):
    def create_stepssection(self, meal, name, content_list):
        steps_section = self.create(
            meal = meal,
            name = name,
        )
        for content in content_list:
            Step.objects.create_step(steps_section, content)
        return steps_section

class StepsSection(models.Model):
    meal = models.ForeignKey(Meal, on_delete=models.CASCADE, related_name='stepssection')
    name = models.CharField(max_length=64)
    def ingredients(self):
        steps = self.step.all()
        ingredients = []
        for step in steps:
            ingredients.append(step.ingredient())
        return ingredients
    def __str__(self):
        return '%s Section on %s (ID: %s)' % (self.name, self.meal.__str__(), self.id)
    objects = StepsSectionManager()

class StepManager(models.Manager):
    def create_step(self, section, content):
        step = self.create(
            section = section,
            content = content,
        )
        return step

class Step(models.Model):
    section = models.ForeignKey(StepsSection, on_delete=models.CASCADE, related_name='step')
    content = models.TextField()
    # attempt to take only the names of each ingredient from the content of this step
    # eg. "3 large eggs" => "egg"
    def ingredient(self):
        # ingredient regexp /((?<=^\d+(\w+| +) +)(\w+) +(\w+))/g
        return re.sub('(^[\d½¼¾⅓⅔]+[\s\S][^ ]* *((tsp)|(g)|(ml)|(tbsp)|(fresh)|(large)|(gluten-free)|(fl oz)|(oz)|(pot)|(tub))* +)|(^\d+ +)|(s* *[,(] *[\s\S][^\n]+)', '', self.content)
    def comment_count(self):
        return self.subcomment.all().count()
    def __str__(self):
        def getThisStepCount(m, l):
            # get the number for this page in relation to other steps on the same page
            c = 1 # count the steps starting at one
            for i in l: # cycle all the steps i in the list l
                if i.pk == m.pk: # when this step is found
                    return c # return the step count
                else:
                    c += 1 # else add one to the count
            else:
                return -1 # if not found return -1
        steps = self.section.step.all()
        thisStep = getThisStepCount(self, list(steps))
        return '%s Step %s of Meal %s (Step ID: %s)' % (self.section.name, thisStep, self.section.meal.name, self.id)
    objects = StepManager()

# Model and manager of a sub-comment, listed under an ingredient or instruction
class SubCommentManager(models.Manager):
    def create_subcomment(self, parent, user, content):
        comment = self.create(
            parent = parent,
            user = user,
            content = content,
        )
        SubCommentRatingSection.objects.create_ratingsection('overall', comment)
        return comment

class SubComment(models.Model):
    parent = models.ForeignKey(Step, on_delete=models.CASCADE, related_name='subcomment')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='subcomment')
    date = models.DateTimeField(auto_now_add=True)
    content = models.TextField()
    def c_name(self):
        return 'subcomment'
    def __str__(self):
        return '%s\'s subcomment on %s (Subcomment ID: %s)' % (self.user.username, self.parent, self.pk)
    class Meta:
        ordering = ('date',)
    objects = SubCommentManager()

# Model and manager of a RatingSection - tracks rating of a meal or a meal's propertyclass MealRatingSectionManager(models.Manager):
class RatingSectionManager(models.Manager):
    def create_ratingsection(self, name, parent):
        return self.create(
            name = name,
            parent = parent,
        )
class AbstractRatingSection(models.Model): # Should never be instantiated.
    name = models.CharField(max_length=20, default='overall')
    def rating_count(self):
        return len(self.rating.all())
    def get_rating(self): # return the average rating or -1 if no votes
        ratings_count = self.rating.count()
        if ratings_count > 0:
            # Round to the nearest whole integer
            return self.rating.aggregate(average_rating=Rnd(Avg('value'))).get('average_rating')
        else:
            return -1
    def __str__(self):
        return '(Rating Section ID: %s) "%s" Rating Section (%s ratings) of %s' % (self.pk, self.name.capitalize(), len(self.rating.all()), self.parent)
    objects = RatingSectionManager()
    class Meta:
        abstract = True

class AbstractCommentRatingSection(AbstractRatingSection): # Should never be instantiated.
    def get_rating(self): # return the total rating or 0 if no votes
        ratings_count = self.rating.count()
        if ratings_count > 0:
            return self.rating.aggregate(final_rating=Sum('value')).get('final_rating')
        else:
            return 0
    class Meta:
        abstract = True

# ratings for meals
class MealRatingSection(AbstractRatingSection):
    parent = models.ForeignKey(Meal, on_delete=models.CASCADE, related_name='ratingsection')

# ratings for comments
class CommentRatingSection(AbstractCommentRatingSection):
    parent = models.OneToOneField(MealComment, on_delete=models.CASCADE, related_name='ratingsection')
class SubCommentRatingSection(AbstractCommentRatingSection):
    parent = models.OneToOneField(SubComment, on_delete=models.CASCADE, related_name='ratingsection')

# Model and manager of a rating
class RatingManager(models.Manager):
    def create_rating(self, parent, user, value):
        rating = self.create(
            parent = parent,
            user = user,
            value = value,
        )
class Rating(models.Model):
    """ DO NOT INSTANTIATE THE Rating CLASS """
    value = models.IntegerField()
    def __str__(self):
        return '(Rating ID: %s) %s\'s rating on %s' % (self.pk, self.user.username, self.parent)
    objects = RatingManager()
    class Meta:
        abstract = True

class MealRating(Rating):
    parent = models.ForeignKey(MealRatingSection, related_name='rating', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='mealrating', on_delete=models.CASCADE)
class CommentRating(Rating):
    parent = models.ForeignKey(CommentRatingSection, related_name='rating', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='commentrating', on_delete=models.CASCADE)
class SubCommentRating(Rating):
    parent = models.ForeignKey(SubCommentRatingSection, related_name='rating', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='subcommentrating', on_delete=models.CASCADE)
