from django import forms

class CommentCreationForm(forms.Form):
    content = forms.CharField(
        label = 'CommentArea',
        widget = forms.TextInput(attrs={
            'class':'form-control',
            'placeholder':'Leave a comment',
        }),
    )
