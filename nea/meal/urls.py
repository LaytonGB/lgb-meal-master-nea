from django.urls import path
from . import views

urlpatterns = [
    path('<int:meal_id>/', views.meal_page, name='meal_page'),
    path('<int:meal_id>/comment/', views.comment, name='comment'),
    path('<int:meal_id>/rate/comment/<int:comment_id>/', views.submit_comment_rating, name='submit_comment_rating'),
    path('<int:meal_id>/rate/subcomment/<int:step_id>/<int:subcomment_id>/', views.submit_subcomment_rating, name='submit_subcomment_rating'),
    path('<int:meal_id>/rate/<str:rating_section_name>/', views.submit_meal_rating, name='submit_meal_rating'),
    path('<int:meal_id>/subcomment/<int:step_id>/', views.subcomment, name='subcomment'),
    path('<int:meal_id>/<str:focus>/', views.meal_page, name='meal_page'),
    path('<int:meal_id>/<str:focus>/<int:step_id>/', views.meal_page, name='meal_page'),
    path('<int:meal_id>/<int:step_id>/', views.meal_page, name='meal_page_subcomment'),
]
