from meal.models import Meal
from django.db import models

from django.contrib.auth.models import User
from django.db.models.aggregates import Max

# Customize the string representation of users
def name(self):
    return f'(ID: {self.id}) User "{self.username}"'

User.add_to_class("__str__", name)

# Users planner model
class PlannerManager(models.Manager):
    def create_planner(self, user):
        planner = self.create(
            user = user,
        )
        return planner
class Planner(models.Model):
    user = models.OneToOneField(User, verbose_name="Meal Planner", on_delete=models.CASCADE)
    def __str__(self):
        return f'(Planner ID: {self.id}) {self.user}\'s Meal Plan'
    class Meta:
        ordering = ('pk',)
    objects = PlannerManager()

class PlannerMealManager(models.Manager):
    def create_plannermeal(self, planner, date, meal, highest_order, custom_meal=None):
        planner_meal = self.create(
            planner = planner,
            date = date,
            meal = meal,
            order = highest_order + 1,
            custom_meal = custom_meal,
        )
        return planner_meal
    def get_highest_order(self, id):
        pm = self.get(id=id) # pm=plannermeal
        return self.filter(date=pm.date).aggregate(order_max=Max('order')).get('order_max') or 0
    def adjust_order(self, x, id):
        # x=new_order y=old_order id=plannermeal_id z=selected_plannermeal
        y = None
        z = None
        try:
            z = self.get(id=id)
            y = z.order
        except:
            pass
        if (not z) or (x is y):
            return
        z.order = self.get_highest_order(id) + 1
        z.save()
        meals_to_modify = None
        a = None # a=adjustment_value
        if x > y:
            meals_to_modify = self.filter(date=z.date).filter(order__gte=y).order_by('order') # plannermeals y or greater
            a = -1
        elif x < y:
            meals_to_modify = self.filter(date=z.date).filter(order__gte=x).order_by('order') # plannermeals x or greater
            a = +1
        for m in meals_to_modify:
            m.order = m.order + a
            m.save()
        z.order = x
        z.save()
class PlannerMeal(models.Model):
    planner = models.ForeignKey(Planner, on_delete=models.CASCADE, related_name='meals')
    date = models.DateField(null=False)
    order = models.IntegerField(unique_for_date='date')
    meal = models.ForeignKey(Meal, verbose_name="Meal(s) in Planner on this Day", related_name='plannermention', on_delete=models.CASCADE, default=1, null=True)
    custom_meal = models.CharField(max_length=40, blank=True, null=True)
    def __str__(self):
        return f'(Planner Meal ID: {self.id}) {self.meal} on {self.date} in {self.planner}'
    class Meta:
        ordering = ('date','order',)
    objects = PlannerMealManager()
