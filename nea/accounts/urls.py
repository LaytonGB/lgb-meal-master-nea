from django.urls import path
from . import views

urlpatterns = [
    path('', views.account_page, name='account_page'),
    path('comments/', views.comments_page, name='comments_page'),
    path('favourites/', views.favourites_page, name='favourites_page'),
    path('login/', views.login_page, name='login'),
    path('logout/', views.logout_user, name='logout'),
    path('planner/', views.planner_page, name='planner_page'),
    path('planner/<str:last_date>/', views.planner_page, name='planner_page'),
    path('planner/add/<int:add_days>/', views.planner_page, name='planner_page'),
    path('planner/<str:last_date>/add/<int:add_days>/', views.planner_page, name='planner_page'),
    path('planner/remove/<int:plannermeal_id>/', views.planner_remove, name='planner_page'),
    path('planner/remove/<str:last_date>/<int:plannermeal_id>/', views.planner_remove, name='planner_page'),
    path('add_favourite/<int:meal_pk>/<path:goto>/', views.add_favourite, name='add_favourite'),
    path('edit_comment/<str:comment_type>/<int:comment_id>/', views.edit_comment, name='edit_comment'),
    path('delete_comment/<str:comment_type>/<int:comment_id>/', views.delete_comment, name='delete_comment'),
]
