from django.db.models.aggregates import Max
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

from itertools import chain
from operator import attrgetter

from django.utils import timezone
from django.utils.dateparse import parse_date
from datetime import datetime, timedelta

from .forms import CreateUserForm, CommentEditForm
from meal.models import Meal, MealComment, SubComment
from .models import Planner, PlannerMeal

def account_page(request):
    context = {'page_title':'My Account',}

    if request.user.is_authenticated:
        return render(request, 'accounts/user_account.html', context)
    else:
        return redirect('/account/login')

def comments_page(request):
    if request.user.is_authenticated:
        user = request.user
        mealcomments = user.mealcomment.all()
        subcomments = user.subcomment.all()
        if mealcomments and subcomments:
            comments = list(mealcomments) + list(subcomments)
            comments.sort(key=lambda a: a.date)
        elif mealcomments:
            comments = mealcomments
        elif subcomments:
            comments = subcomments
        else:
            comments = None

        context = {
            'page_title':'My Comments',
            'user':user,
            'comments':comments,
        }
        return render(request, 'accounts/user_comments.html', context)
    else:
        return redirect('/account/login')

def favourites_page(request):
    if not request.user.is_authenticated:
        return redirect('/account/login')
    user = request.user
    favourites_query = list(user.meal_set.all())
    favourites_list = []
    for f in favourites_query:
        try:
            rating_section = f.ratingsection.get(name='overall')
            meal_rating = rating_section.get_rating()
            rating_count = rating_section.rating_count()
        except:
            rating_count = 0
            meal_rating = -1
        favourites_list.append({
            'id':f.id,
            'name':f.name,
            'description':f.description,
            'rating_count':rating_count,
            'rating':meal_rating,
        })
    context = {
        'page_title':'Favourites',
        'user':user,
        'favourites':favourites_list,
    }
    return render(request, 'accounts/user_favourites.html', context)

def planner_page(request, last_date='', add_days=0):
    user = request.user

    if not user.is_authenticated:
        return redirect('/account/login')

    # set date to user input if there is any, else today
    last_date_parsed = None
    if last_date:
        last_date_parsed = datetime.strptime(last_date, "%d-%m-%Y").date()
    date = last_date_parsed or timezone.now()
    userdate = request.GET.get('submitted_date')
    if userdate:
        # this kind of parsing only works because the date is supplied straight from a HTML calendar
        date = parse_date(userdate)
        last_date = datetime.strftime(date, "%d-%m-%Y")
        if last_date:
            return redirect(f'/account/planner/{last_date}')
        return redirect('/account/planner/')

    # if a meal needs to be added, add it to the specific day
    # then reload this view
    data = request.GET.get('add_meal')
    if data:
        parts = data.split()
        sub_date = datetime.strptime(parts[0], "%d-%m-%Y").date()
        meal_id = parts[1]
        meal = Meal.objects.get(id=meal_id)
        if meal:
            order = PlannerMeal.objects.filter(date=sub_date).aggregate(order_max=Max('order')).get('order_max') or 0
            PlannerMeal.objects.create_plannermeal(user.planner, sub_date, meal, order)
        if last_date:
            return redirect(f'/account/planner/{last_date}')
        return redirect('/account/planner/')

    # if a custom meal needs to be added, add it to the specific day
    # then reload this view
    data = request.GET.get('custom_meal_name')
    if data:
        add_date_parsed = date + timedelta(days=add_days)
        order = PlannerMeal.objects.filter(date=add_date_parsed).aggregate(order_max=Max('order')).get('order_max') or 0
        PlannerMeal.objects.create_plannermeal(user.planner, add_date_parsed, None, order, data)
        if last_date:
            return redirect(f'/account/planner/{last_date}')
        return redirect('/account/planner/')

    # if a meal order is to be changed, change it
    # then reload this view
    data = request.GET.get('change_meal_order')
    if data:
        parts = data.split()
        new_order = int(parts[0])
        plannermeal_id = int(parts[1])
        if plannermeal_id and new_order:
            PlannerMeal.objects.adjust_order(new_order, plannermeal_id)
        else:
            messages.warning(request, 'Could not change meal order.')
        if last_date:
            return redirect(f'/account/planner/{last_date}')
        return redirect('/account/planner/')

    # get user favourites
    faves = user.meal_set.all()
    favourites = []
    for f in faves:
        favourites.append({
            'id':f.id,
            'name':f.name,
            'description':f.description,
            'rating':f.ratingsection.get(name='overall').get_rating(),
            'rating_count':f.ratingsection.get(name='overall').rating_count(),
        })

    # if user does not have a planner, create one
    try:
        user.planner
    except:
        Planner.objects.create_planner(user)

    # get meals from user planner and add them to the necessary days
    planner = []
    dates = []
    for i in range(0, 7): # for each day
        day = []
        this_date = date + timedelta(days=i)
        dates.append(this_date)
        meals = user.planner.meals.filter(date=this_date).order_by('order')
        if meals:
            for m in meals: # for each mealplan_meal
                # try and add the meal as a proper meal. if the meal is custom, this will cause an error
                # in which case, add the meal as a custom meal
                try:
                    day.append({
                        'id':m.id,
                        'order':m.order,
                        'meal_id':m.meal.id,
                        'name':m.meal.name,
                        'description':m.meal.description,
                        'rating':m.meal.ratingsection.get(name='overall').get_rating(),
                        'rating_count':m.meal.ratingsection.get(name='overall').rating_count(),
                    })
                except:
                    day.append({
                        'id':m.id,
                        'order':m.order,
                        'meal_id':-1,
                        'name':m.custom_meal,
                        'description':'',
                        'rating':-1,
                        'rating_count':-1,
                    })
        planner.append(day)

    context = {
        'page_title':'Meal Planner',
        'user':request.user,
        'date':date,
        'planner':planner,
        'dates':dates,
        'favourites':favourites,
        'last_date':last_date,
    }
    return render(request, 'accounts/meal_planner.html', context)

def planner_remove(request, plannermeal_id, last_date=''):
    if plannermeal_id:
        PlannerMeal.objects.get(id=plannermeal_id).delete()
    else:
        messages.warning(request, 'Could not remove meal.')
    if last_date:
        return redirect(f'/account/planner/{last_date}')
    return redirect('/account/planner/')

def delete_comment(request, comment_type, comment_id):
    if request.user.is_authenticated:
        if comment_type == 'mealcomment':
            comment = MealComment.objects.get(pk=comment_id)
        elif comment_type == 'subcomment':
            comment = SubComment.objects.get(pk=comment_id)
        else:
            messages.error(request, 'Comment could not be found.')
            return redirect('/account/comments')
        user = request.user
        if user == comment.user:
            comment.delete()
            messages.success(request, 'Comment deleted.')
            return redirect('/account/comments')
        else:
            return redirect('/account/login')

    else:
        return redirect('/account/login')

def edit_comment(request, comment_type, comment_id):
    if comment_type == 'mealcomment':
        comment = MealComment.objects.get(pk=comment_id)
    elif comment_type == 'subcomment':
        comment = SubComment.objects.get(pk=comment_id)
    else:
        messages.error(request, 'Comment could not be found.')
        return redirect('/account/comments')

    user = request.user
    if request.method == 'POST':
        new_content = request.POST.get('content')
        comment.content = new_content
        comment.save()
        messages.success(request, 'Comment has been edited.')
        return redirect('/account/comments')

    elif user.is_authenticated:
        if user == comment.user:
            form = CommentEditForm()
            context = {
                'form':form,
                'page_title':'Edit Comment',
                'comment':comment,
                'user':user,
            }
            return render(request, 'accounts/edit_comment.html', context)
        else:
            return redirect('/account/login')

    else:
        return redirect('/account/login')

def add_favourite(request, meal_pk, goto):
    if request.method == 'POST':
        try:
            user = request.user
            meal = Meal.objects.get(pk=meal_pk)
            if meal.favourites.filter(id=user.id).first():
                meal.favourites.remove(user)
            else:
                meal.favourites.add(user)
        except:
            messages.warning(request, 'Could not add favourite.')
    # redirect to previous page
    return redirect(goto)

def login_page(request):
    form = CreateUserForm()
    context = {'page_title':'Login or Register', 'form':form,}

    if request.method == "POST":
        if "login" in request.POST:
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('/account')
            else:
                messages.error(request, 'Username or password incorrect.')
                return render(request, 'accounts/login.html', context)

        if "register" in request.POST:
            form = CreateUserForm(request.POST)

            if form.is_valid():
                form.save()
                username = form.cleaned_data.get('username')
                messages.success(request, 'Account successfully created.\nWelcome to MealMaster, '
                                + username + '!')
                return redirect('/account/login')

    context = {'page_title':'Login or Register', 'form':form,}
    return render(request, 'accounts/login.html', context)

def logout_user(request):
    logout(request)
    messages.success(request, 'You have been successfully logged out.')
    return redirect('/account/login')
