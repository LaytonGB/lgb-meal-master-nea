from django.contrib import admin

from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from meal.admin import FavouritesInline, MealCommentInlineLink, SubCommentInlineLink

class CustomUserAdmin(UserAdmin):
    def __init__(self, *args, **kwargs):
        super(UserAdmin,self).__init__(*args, **kwargs)
        UserAdmin.list_display = list(UserAdmin.list_display)
        UserAdmin.inlines = list(UserAdmin.inlines) + [FavouritesInline, MealCommentInlineLink, SubCommentInlineLink]
admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)
