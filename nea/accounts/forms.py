from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from meal.forms import CommentCreationForm

class CreateUserForm(UserCreationForm):
    password1 = forms.CharField(
        label=('Password'),
        strip=False,
        widget=forms.PasswordInput(attrs={'class':'form-control', 'placeholder':'Password',})
    )
    password2 = forms.CharField(
        label=('Confirm Password'),
        strip=False,
        widget=forms.PasswordInput(attrs={'class':'form-control', 'placeholder':'Confirm Password',})
    )

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']
        widgets = {
            'username': forms.TextInput(attrs={'class':'form-control', 'placeholder':'Username',}),
            'email': forms.TextInput(attrs={'class':'form-control', 'placeholder':'Email',}),
        }

class CommentEditForm(forms.Form):
    content = forms.CharField(
        label = 'CommentArea',
        widget = forms.TextInput(attrs={
            'class':'form-control',
            'id':'comment_input',
            'placeholder':'Leave a comment',
        }),
    )
