from django.shortcuts import render

def welcome(request):
    page_name = 'Home'
    if request.user.is_authenticated:
        username = request.user.username
    else:
        username = 'Logged-out User'

    context = {'page_title':'Welcome', 'username':username,}
    return render(request, 'welcome/welcome.html', context)

def home(request):
    context = {'page_title':'Home'}
    return render(request, 'welcome/home.html', context)

def about(request):
    context = {'page_title':'About'}
    return render(request, 'welcome/about.html', context)
