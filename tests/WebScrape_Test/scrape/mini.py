"""
Module Docstring
"""

from typing import List

import requests
from bs4 import BeautifulSoup
# bs4 documentation is at https://www.crummy.com/software/BeautifulSoup/bs4/doc/

print('Enter a search term:')
search_term = input()

search = 'https://www.bbcgoodfood.com/search/recipes?query=' + search_term
print(search)
page = requests.get(search, headers={'User-Agent':'Chrome/84'})
soup = BeautifulSoup(page.content, 'html.parser')

results = soup.find_all('h3', class_='teaser-item__title')

for result in results:
    print(result.get_text())
