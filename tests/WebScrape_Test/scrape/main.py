"""
Module Docstring
"""

import requests
from bs4 import BeautifulSoup
# bs4 documentation is at https://www.crummy.com/software/BeautifulSoup/bs4/doc/

class Meal:
    """Class for any meal."""

    def __init__(self, URL_in: BeautifulSoup) -> None:
        page = requests.get(URL_in, headers={"User-Agent":"Chrome/84"})
        self.soup = BeautifulSoup(page.content, 'html.parser')

    def print(self) -> None:
        """Prints the string."""
        print('\n\n=== ' + self.soup.h1.get_text('', strip=True).upper() + ' ===')
        print('= Ingredients ='.upper())
        for ingredient in self.soup.find_all(attrs={'class':'ingredients-list__item'}):
            print(ingredient['content'])
        print('= Instructions ='.upper())
        inst_num = 1
        for instruction in self.soup.find_all(attrs={'itemprop':'recipeInstructions'}):
            print(str(inst_num) + '. ' + instruction.p.get_text('', strip=True))
            inst_num += 1

class MealSearch:
    """Class for the results of a meal search."""

    def __init__(self, search_term: str) -> None:
        search = 'https://www.bbcgoodfood.com/search/recipes?query=' + search_term
        page = requests.get(search, headers={'User-Agent':'Chrome/84'})
        self.soup = BeautifulSoup(page.content, 'html.parser')
        self.results = []
        for result in self.soup.find_all('h3', class_='teaser-item__title'):
            result_text = str(result.get_text('', strip=True))
            result_url = str(result.a.get('href'))
            # print('------\nText: ' + result_text + '\nURL: ' + result_url)
            self.results.append([result_text, result_url])

    def print(self) -> None:
        """Prints the list of found meals."""
        print('\n=== Search Results ==='.upper())
        result_num = 1
        for result in self.results:
            print(str(result_num) + '. ' + result[0])
            result_num += 1

    def get_meal_url(self, choice: int) -> str:
        """Returns the URL of the selected Meal."""
        return self.results[choice][1]

print('\n\nEnter a BBC Good Foods search term:')
user_input = input()
results = MealSearch(user_input)
results.print()

print('\nSelect from the options above by entering the number of your choice:')
user_input = input()
user_choice = int(user_input) - 1
user_choice_url = results.get_meal_url(user_choice)
chosen_meal = Meal('https://www.bbcgoodfood.com/' + user_choice_url)
chosen_meal.print()
