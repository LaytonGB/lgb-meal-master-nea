SELECT DISTINCT meal_meal.name
FROM meal_meal
INNER JOIN meal_stepssection ON meal_meal.id=meal_stepssection.meal_id
INNER JOIN meal_step ON meal_stepssection.id=meal_step.section_id
WHERE meal_stepssection.name='Ingredients' AND INSTR(LOWER(meal_step.content), 'milk')<>0;
